package com.douglasanunciacao.dribble;

import android.os.Bundle;

import com.douglasanunciacao.dribble.fragment.FeedFragment;

import it.neokree.materialnavigationdrawer.MaterialAccount;
import it.neokree.materialnavigationdrawer.MaterialAccountListener;
import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;

public class MainActivity extends MaterialNavigationDrawer implements MaterialAccountListener {

    @Override
    public void init(Bundle bundle) {

        MaterialAccount account = new MaterialAccount(getResources(), "Douglas", "douglas.samuel@gmail.com", R.drawable.profile, R.drawable.background_linkedin);
        this.addAccount(account);

        this.setAccountListener(this);

        this.addSection(newSection("Popular", FeedFragment.newInstance("")));

        this.setBackPattern(MaterialNavigationDrawer.BACKPATTERN_BACK_TO_FIRST);

    }

    @Override
    public void onAccountOpening(MaterialAccount materialAccount) {

    }

    @Override
    public void onChangeAccount(MaterialAccount materialAccount) {

    }
}
