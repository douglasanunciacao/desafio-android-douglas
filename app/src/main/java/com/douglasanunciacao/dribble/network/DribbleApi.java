/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble.network;

import com.douglasanunciacao.dribble.model.Shot;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by douglasanunciacao 18/09/2015.
 */
public interface DribbleApi {

    public static final String END_POINT = "https://api.dribbble.com/v1";

    @GET("/shots")
    public Observable<List<Shot>> shots(@Query("list") String list, @Query("page") Integer page);

    @GET("/shots/{id}/attachments")
    public void attachments(@Path("id") String shotId, Callback callback);

    @GET("/shots/{shot}/comments")
    public void comments(@Path("shot") String shotId, Callback callback);
}
