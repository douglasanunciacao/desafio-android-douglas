/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble.fragment;

import android.accounts.Account;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by douglasanunciacao on 18/09/2015.
 */
public class BaseFragment extends Fragment {

    private Handler mHandler = new Handler();
    protected Context mContext;
    protected Account mAccount;

    private ProgressDialog mProgressDialog;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int viewId = onGetViewId();
        if (viewId > 0) {
            View view = inflater.inflate(viewId, null, false);
            ButterKnife.inject(this, view);
            return view;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        ButterKnife.reset(this);  // 页面切换过快会出现空指针
    }

    protected int onGetViewId() {
        return 0;
    }

    public void startActivity(Class<? extends Activity> activity) {
        Intent intent = new Intent(getActivity(), activity);
        startActivity(intent);
    }

    public void startActivityForResult(Class<? extends Activity> activity,
                                       int requestCode) {
        Intent intent = new Intent(getActivity(), activity);
        startActivityForResult(intent, requestCode);
    }

    protected void addFragment(int containerId, Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(containerId, fragment);
        transaction.commit();
    }

    protected void replaceFragment(int containerId, Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(containerId, fragment);
        transaction.commit();
    }

    public void post(Runnable task) {
        mHandler.post(task);
    }

    public void post(Runnable task, long delay) {
        mHandler.postDelayed(task, delay);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null && fragments.size() > 0) {
            Fragment fragment = getChildFragmentManager().getFragments().get(0);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
