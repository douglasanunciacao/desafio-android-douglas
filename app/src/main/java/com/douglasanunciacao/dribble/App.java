/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble;

import android.app.Application;

import com.squareup.picasso.Picasso;

import timber.log.Timber;

/**
 *  Created by douglasanunciacao on 18/09/2015.
 */
public class App extends Application {

    public static App instance;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        Picasso.with(this).setIndicatorsEnabled(BuildConfig.DEBUG);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
