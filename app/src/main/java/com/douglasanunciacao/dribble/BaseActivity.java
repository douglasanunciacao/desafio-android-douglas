/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.douglasanunciacao.dribble.util.ToastUtil;

import butterknife.ButterKnife;

/**
 * Created by douglasanunciacao 18/09/2015.
 */
public class BaseActivity extends ActionBarActivity {

    private Handler mHandler = new Handler();
    public BaseActivity mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        int viewId = onGetViewId();

        if (viewId > 0) {
            setContentView(viewId);
            ButterKnife.inject(this);
            onViewCreated();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ButterKnife.reset(this);
    }

    protected int onGetViewId() {
        return 0;
    }

    protected void onViewCreated() {

    }

    public void startActivity(Class<? extends Activity> activity) {
        Intent intent = new Intent(mContext, activity);
        startActivity(intent);
    }

    public void startActivityAndFinish(Class<? extends Activity> activity) {
        startActivity(activity);
        finish();
    }

    public void startActivityAndFinish(Intent intent) {
        startActivity(intent);
        finish();
    }

    public void startActivityForResult(Class<? extends Activity> activity,
                                       int requestCode) {
        Intent intent = new Intent(mContext, activity);
        startActivityForResult(intent, requestCode);
    }

    public void showToast(String text) {
        ToastUtil.showShort(this, text);
    }

    public void showToast(int strId) {
        ToastUtil.showShort(this, strId);
    }

    public void post(Runnable task) {
        mHandler.post(task);
    }

    public void post(Runnable task, long delay) {
        mHandler.postDelayed(task, delay);
    }

    public void remove(Runnable task) {
        mHandler.removeCallbacks(task);
    }


    protected void addFragment(int containerId, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(containerId, fragment);
        transaction.commit();
    }

    protected void replaceFragment(int containerId, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerId, fragment);
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
