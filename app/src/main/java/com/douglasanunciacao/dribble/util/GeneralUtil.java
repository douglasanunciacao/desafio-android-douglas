package com.douglasanunciacao.dribble.util;

import com.douglasanunciacao.dribble.model.Shot;

/**
 * Created by douglas on 19/09/2015.
 */
public class GeneralUtil {

    public static String getShotImageUrl(Shot shot) {

        if (shot.getImages().getHidpi() != null)
            return shot.getImages().getHidpi();
        else  if (shot.getImages().getNormal() != null)
            return shot.getImages().getNormal();
        else  if (shot.getImages().getTeaser() != null)
            return shot.getImages().getTeaser();

        return null;

    }

}
