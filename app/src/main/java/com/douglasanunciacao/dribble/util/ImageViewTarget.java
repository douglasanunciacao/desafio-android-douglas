/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble.util;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by douglasanunciacao on 18/09/2015.
 */
public class ImageViewTarget implements Target {

    private ImageView mImageView;

    public ImageViewTarget(ImageView imageView) {
        mImageView = imageView;
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        mImageView.setImageBitmap(bitmap);
//        ByteBuffer buffer = ByteBuffer.allocate(bitmap.getByteCount());
//        bitmap.copyPixelsToBuffer(buffer);
//        try {
//            GifDrawable drawable = new GifDrawable(buffer);
//            mImageView.setImageDrawable(drawable);
//            MediaController mc = new MediaController(mImageView.getContext());
//            mc.setMediaPlayer( ( GifDrawable ) mImageView.getDrawable() );
//            mc.setAnchorView(mImageView);
//            mc.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {

    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }
}
