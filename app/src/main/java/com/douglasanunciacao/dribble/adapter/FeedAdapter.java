/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.douglasanunciacao.dribble.AppModule;
import com.douglasanunciacao.dribble.DetailActivity;
import com.douglasanunciacao.dribble.R;
import com.douglasanunciacao.dribble.model.Shot;
import com.douglasanunciacao.dribble.util.GeneralUtil;
import com.douglasanunciacao.dribble.widget.SwipeHoverLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by douglasanunciacao 18/09/2015.
 */
public class FeedAdapter extends BaseAdapter<FeedAdapter.ViewHolder> {

    private List<Shot> mList;

    @Inject
    Picasso mPicasso;

    public FeedAdapter(List<Shot> list) {
        this.mList = list;
        if (mList == null)
            mList = new ArrayList<Shot>();

        AppModule.inject(this);
    }

    public void addList(List<Shot> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Shot shot = mList.get(position);
        mPicasso.load(shot.getUser().getAvatar_url()).into(holder.mIvAvatar);
        holder.mTvUsername.setText(shot.getUser().getName());
        holder.mTitleTv.setText(shot.getTitle());
        holder.mTvViews.setText(shot.getViews_count().toString());

        String shotImageUrl = GeneralUtil.getShotImageUrl(shot);
        mPicasso.load(shotImageUrl).into(holder.mIvImage);

        holder.mTvDescription.setText(Html.fromHtml(shot.getDescription()));
        holder.mHoverLayout.destroyHover();
        holder.mIvImage.setTag(shot);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends BaseAdapter.ViewHolder {

        @InjectView(R.id.iv_avatar)
        ImageView mIvAvatar;
        @InjectView(R.id.tv_username)
        TextView mTvUsername;
        @InjectView(R.id.tv_title)
        TextView mTitleTv;
        @InjectView(R.id.iv_image)
        ImageView mIvImage;
        @InjectView(R.id.tv_views)
        TextView mTvViews;
        @InjectView(R.id.tv_description)
        TextView mTvDescription;
        @InjectView(R.id.hoverLayout)
        SwipeHoverLayout mHoverLayout;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        @OnClick(R.id.iv_image)
        public void onClick(View view) {
            DetailActivity.start((android.app.Activity) view.getContext(), (Shot) view.getTag(), view);
        }
    }
}
