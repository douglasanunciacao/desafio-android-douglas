/*
 * @author douglasanunciacao 18.09.2015 douglas.samuel@gmail.com
 */
package com.douglasanunciacao.dribble.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.douglasanunciacao.dribble.util.DimenUtil;

import butterknife.ButterKnife;

/**
 * Created by douglasanunciacao 18/09/2015.
 */
public abstract class BaseAdapter<VH extends BaseAdapter.ViewHolder> extends RecyclerView.Adapter<VH> {

    private int mLastPosition = -1;

    @Override
    public void onBindViewHolder(VH holder, int position) {
        if (position > mLastPosition) {
            mLastPosition = position;
            startItemAnimation(holder, position);
        }
    }

    protected void startItemAnimation(VH holder, int position) {
        holder.mItemView.setTranslationY(DimenUtil.getScreenWidth(holder.mItemView.getContext()) / 2);
        holder.mItemView.setAlpha(0);
        holder.mItemView.animate().translationY(0).alpha(1).setDuration(500).start();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View mItemView;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;
            ButterKnife.inject(this, itemView);
        }
    }
}
